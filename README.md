# Requirements: 
[Node.js](https://nodejs.org/en/)

I've only tested on node.js v6.9.1, However this should work on most node versions.
# Usage:

git clone this repo into any directory then run:
```
npm install
npm run build
npm start
```

then navigate to http://localhost:3001/plug-room-slug for room state, add /history to the end of the url for the rooms history